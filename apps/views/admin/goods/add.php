<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加 （返佣说明 0代表关闭，输入数字代表 百分比，数字后面带元字 表示直接返现金）</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">定位区域</label>
				<div class="layui-input-block">
					<span class="layui-btn layui-btn-primary" id="add_location">添加</span>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">商家</label>
				<div class="layui-input-block">
					<select name="data[sid]" >
	                <?php foreach($shop as $v){?>
	                	<option value="<?php echo $v['id'];?>"> <?php echo $v['sname'];?></option>
	                <?php }?>
	                </select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">分类</label>
				<div class="layui-input-block">
					<select name="data[ggid]" >
	                <?php foreach($group as $v){?>
	                	<option value="<?php echo $v['id'];?>"> <?php echo $v['ggname'];?></option>
	                <?php }?>
	                </select>
				</div>
			</div>
						
			<div class="layui-form-item">
				<label class="layui-form-label">名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[gname]" class="layui-input" lay-verify="required">
				</div>
			</div>
			
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">市场价</label>
					<div class="layui-input-inline">
						<input type="text" name="data[mprice]" value="0" class="layui-input"  lay-verify="required|number">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">现售价</label>
					<div class="layui-input-inline">
						<input type="text" name="data[price]" value="0" class="layui-input"  lay-verify="required|number">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">库存</label>
					<div class="layui-input-inline">
						<input type="text" name="data[stock]" class="layui-input" value="100" lay-verify="number"> 
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">开始时间</label>
					<div class="layui-input-inline">
						<input type="text" name="data[stime]" id="stime" class="layui-input time"  lay-verify="required">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">结束时间</label>
					<div class="layui-input-inline">
						<input type="text" name="data[etime]" id="etime"  class="layui-input time" lay-verify="required"> 
					</div>
				</div>
			</div>
			
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">限购</label>
					<div class="layui-input-inline">
						<input type="text" name="data[xg]" class="layui-input" value="0" placeholder="0为不限制" lay-verify="required">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">积分</label>
					<div class="layui-input-inline">
						<input type="text" name="data[integral]" class="layui-input" value="0" lay-verify="required">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">积分兑换</label>
					<div class="layui-input-inline">
						<input type="text" name="data[exchange]" class="layui-input" placeholder="输入0.01代表1积分等于1分" lay-verify="required">
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">是否推荐</label>
				<div class="layui-input-block">
					<input type="checkbox" name="data[tj]" lay-skin="switch" lay-text="开启|关闭" value="1" checked="checked"> 
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">商品图</label>
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">640px * 330px</span>
					<div class="layui-upload layui-hide">
					  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
					  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
				  	</div>
				  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify="thumb"/>
				</div>
			</div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">商品详情</label>
				<div class="layui-input-block">
					<textarea class="layui-textarea" name="data[say]" ></textarea>
				</div>
			</div>
												
			<div class="layui-form-item">
				<label class="layui-form-label">商品详情</label>
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">图片最大宽度640px 高度随意（小程序不支持富文本，只能用图片代替）</span>
					<div class="layui-upload layui-hide">
					  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
					  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
				  	</div>
				  	<input name="data[content]" class="thumb" type="hidden" />
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">一级返佣</label>
					<div class="layui-input-inline">
						<input type="text" name="data[f_1]" value="0" class="layui-input"  lay-verify="required">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">二级返佣</label>
					<div class="layui-input-inline">
						<input type="text" name="data[f_2]" value="0" class="layui-input"  lay-verify="required">
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">三级返佣</label>
					<div class="layui-input-inline">
						<input type="text" name="data[f_3]"  value="0" class="layui-input" lay-verify="required"> 
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url' ")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
	$('.time').each(function(){
		layui.laydate.render({ 
			  elem: this,
			  format:'yyyy-MM-dd HH:mm'
		});	
	});
	$('#add_location').click(function(){
		var that = this;
		layer.open({
			type:2,
			content:'/adminct/location/iframe',
			area: ['60%', '60%'],
			btn:'选择插入', 
			btnAlign: 'c',
			yes:function(index){
				var body = layer.getChildFrame('body', index);
				$(body.find("input[name='xz']:checked")).each(function(k) {
					var name = $(this).data('name');
                    var id = $(this).val();
                    var h = '<input type="checkbox" name="data[lid]['+id+']" class="xl" title="'+name+'" value="'+id+'" checked>'
                    $(that).before(h);
                    layui.form.render('checkbox');
                });
				layer.closeAll('iframe');
			}
		});
	});
});
</script>
<?php echo template('admin/footer');?>