<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 积分
 * @author chaituan@126.com
 */
class Integral extends AdminCommon {
	function __construct() {
		parent::__construct();
		$this->load->model(array('admin/Integral_model'=>'do'));
	}
		
	public function index() {
		$this->load->view ('admin/integral/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');		
		$where = $name?"user.nicknames like '%$name%'":'';
		$data = $this->do->getItems_join(array('user'=>"integral.uid=user.id+left"),$where,'integral.*,user.nickname','integral.id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		$data = get_Nickname($data);
		f_ajax_lists($total, $data);
	}	
	
	public function del() {
		$id = Gets('id');
		$r = $this->do->deletes("id=$id");
		if ($r) {
			$I = $this->do->getItem(array('id'=>$id));
			if($I['ands']=='+'){
				$where = array('integral'=>'-='.$data['integral']);
			}else{
				$where = array('integral'=>'+='.$data['integral']);
			}
			$this->User_model->updates($where,array('id'=>$I['uid']));
			AjaxResult_ok ();
		} else {
			AjaxResult_error ();
		}
	}
}
