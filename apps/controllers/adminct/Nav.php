<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 首页导航
 * @author chaituan@126.com
 */
class Nav extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/Nav_model','do');
		$this->load->vars('cat',array('0'=>'请选择','1'=>'栏目','2'=>'商品','3'=>'推荐'));
	}
	
	public function index() {
		$this->load->view('admin/nav/index');
	}
	
	function lists(){
		$srk = Gets('srk');$num = Gets('num');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');$total = Gets('total','num');
		$where = '';
		if($srk)$where['nname like'] = "%$srk%";
		$data = $this->do->getItems($where,'','sort,id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($srk&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$cid = $data['cid'];
			$cidval = Posts('cidval');
			if($cid==1){
				$data['url'] = "/pages/lists/index?id=$cidval&tj=0";
			}elseif ($cid==2){
				$data['url'] = "/pages/goods/detail?id=$cidval";
			}elseif ($cid==3){
				$data['url'] = "/pages/lists/index?id=$cidval&tj=1";
			}
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/nav/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			$cid = $data['cid'];
			$cidval = Posts('cidval');
			if($cid==1){
				$data['url'] = "/pages/lists/index?id=$cidval&tj=0";
			}elseif ($cid==2){
				$data['url'] = "/pages/goods/detail?id=$cidval";
			}elseif ($cid==3){
				$data['url'] = "/pages/lists/index?id=$cidval&tj=1";
			}
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id'=>Gets('id','num')));
			$this->load->view('admin/nav/edit',$data);
		}
	}
	
	function edits(){
		if(is_ajax_request()){
			$data = Posts('data');
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
