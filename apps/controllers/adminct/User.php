<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 会员列表
 * @author chaituan@126.com
 */
class User extends AdminCommon {
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/User_model'=>'do'));
	}
	
	public function config() {
		$data = get_Cache('admin_config');
		foreach ( $data as $v ) {
			if ($v ['tkey'] == 'user') {
				$datas ['items'][] = $v;
			}
		}
		$this->load->view ('admin/config/views', $datas);
	}
	
	public function fx() {
		$this->load->view ('admin/user/fx');
	}
	
	function fx_lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"gid in (2,3) and nicknames like '%$name%'":'gid in (2,3)';
		$data = $this->do->getItems ($where,'id,nicknames,addtime,thumb,gid','gid desc',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	function sh_ok(){
		$id = Gets('id');
		$result = $this->do->updates(array('gid'=>2),"id=$id");
		is_AjaxResult($result);
	}
	
	function sh_no(){
		$id = Gets('id');
		$result = $this->do->updates(array('gid'=>1),"id=$id");
		is_AjaxResult($result);
	}
	
	function fx_del(){
		$id = Gets('id');
		$where = array('gid'=>1);
		$result = $this->do->updates($where,"id=$id");
		is_AjaxResult($result);
	}
	
	public function index() {
		$this->load->view ('admin/user/index');
	}
	
	//页面table获取数据
	function lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"nicknames like '%$name%'":'';
		$data = $this->do->getItems ($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		
		f_ajax_lists($total, get_Nickname($data));
	}
	
	function del() {
		sleep(1);
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
	function lock(){
		sleep(1);
		$id = Gets('id','checkid');
		$open = Gets('open','checkid');
		$result = $this->do->updates(array('state'=>$open),array('id'=>$id));
		is_AjaxResult($result);
	}
	
	function dels(){
		sleep(1);
		$data = Posts();
		if (!$data)AjaxResult_error('没有选中要删除的');
		$ids = implode(',', $data['checked']);
		$result = $this->do->deletes("id in ($ids)");
		is_AjaxResult($result);
	}
	
	public function edit() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		} else {
			$id = Gets("id","checkid");
			$data['item'] = $this->do->getItem("id=$id");
			$this->load->view ( 'admin/user/edit', $data );
		}
	}
}
