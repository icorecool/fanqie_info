<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 商家管理
 * @author chaituan@126.com
 */
class Shop extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model(array('admin/Shop_model'=>'do'));
	}
	
	public function index() {
		$this->load->view ('admin/shop/index');
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"sname like '%$name%'":'';
		$data = $this->do->getItems($where,'','id desc',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			$data['pwd'] = md5(md5($data['pwd']));
			is_AjaxResult ( $this->do->add($data));
		} else {
			$this->load->view('admin/shop/add');
		}
	}
	
	function edits(){
		if(is_ajax_request()){
			$data = Posts('data');
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		}
	}
	
	public function edit() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if($data['pwd']){
				$data['pwd'] = md5(md5($data['pwd']));
			}else{
				unset($data['pwd']);
			}
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		} else {
			$id = Gets("id","checkid");
			$data['item'] = $this->do->getItem("id=$id");
			$this->load->view ( 'admin/shop/edit', $data );
		}
	}
	
	function del() {
		$id = Gets ('id','checkid');
		$result = $this->do->deletes(array('id'=>$id));
		is_AjaxResult($result);
	}
	
}
