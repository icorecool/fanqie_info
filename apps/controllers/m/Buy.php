<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Buy extends CI_Controller {
	
	function index(){
		$timediff = strtotime('2018-11-11') - time();
		$days = intval($timediff/86400);
		//计算小时数
		$remain = $timediff%86400;
		$hours = intval($remain/3600);
		//计算分钟数
		$remain = $remain%3600;
		$mins = intval($remain/60);
		//计算秒数
		$secs = $remain%60;
		$data['time'] = $timediff;
		$this->load->view('mobile/buy/index',$data);
	}


}
