<?php defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class Card_api extends CI_Controller {
	
	function card_update() {
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Card_model'=>'do'));
			$card_data = array(
					'uid'=>$data['uid'],
					'username'=>$data['username'],
					'mobile'=>$data['mobile'],
					'company'=>$data['company'],
					'bm'=>$data['bm'],
					'position'=>$data['position'],
					'zz'=>$data['zz'],
					'address'=>$data['address'],
					'la'=>$data['la'],
					'lg'=>$data['lg'],
					'iid'=>$data['iid'],
					'iname'=>$data['iname'],
					'cid'=>$data['cid'],
					'cname'=>$data['cname'],
					'thumb'=>$data['thumb'],
					'addtime'=>time()
			);
			if($data['query']=='edit'){
				unset($card_data['addtime']);
				$result = $this->do->updates($card_data,array('id'=>$data['id']));
			}else{
				$result = $this->do->add($card_data);				
			}
			is_AjaxResult($result,$result);
		}
	}
	
	
	function card (){
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Card_model'=>'do'));
			$item = $this->do->getItem(array('uid'=>$data['uid']));
			
			if($item){
				$item['thumb'] = base_url($item['thumb']);
				AjaxResult(1, '',$item);
			}else{
				AjaxResult_error();
			}
		}
	}
	//谁赞我 我赞谁
	function card_zan_lists(){
		if(is_ajax_request()){
			$data = Posts();
			if($data['state']==1){//谁赞了我
				$where = array('o_uid'=>$data['uid']);
				$fields = "uid,nickname,header,position,addtime";
			}else{//我赞了谁
				$where = array('uid'=>$data['uid']);
				$fields = "o_uid as uid,o_nickname as nickname,o_header as header,o_position as position,addtime";
			}
			$this->load->model(array('admin/CardZan_model'=>'do'));
			
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$items = $this->do->getItems($where,$fields,'id desc',$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			if($items){
				foreach ($items as $v){
					$v['addtime'] = time_ago($v['addtime']);
					$news[] = $v;
				}
				AjaxResult_page($news,$pagemenu);
			}else{
				AjaxResult_error('暂无数据~~~~(>_<)~~~~');
			}
		}
	}
	//我关注谁 谁关注我
	function card_follow_lists(){
		if(is_ajax_request()){
			$data = Posts();
			if($data['state']==1){//谁关注了我
				$where = array('o_uid'=>$data['uid']);
				$fields = "uid,nickname,header,position,addtime";
			}else{//我关注了谁
				$where = array('uid'=>$data['uid']);
				$fields = "o_uid as uid,o_nickname as nickname,o_header as header,o_position as position,addtime";
			}
			$this->load->model(array('admin/CardFollow_model'=>'do'));
			
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$items = $this->do->getItems($where,$fields,'id desc',$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			if($items){
				foreach ($items as $v){
					$v['addtime'] = time_ago($v['addtime']);
					$news[] = $v;
				}
				AjaxResult_page($news,$pagemenu);
			}else{
				AjaxResult_error('暂无数据~~~~(>_<)~~~~');
			}
		}
	}
	//人气列表
	function card_rq_lists(){
		if(is_ajax_request()){
			$data = Posts();
			if($data['state']==1){
				$where = array('o_uid'=>$data['uid']);
				$fields = "uid,nickname,header,position,addtime";
			}else{
				$where = array('uid'=>$data['uid']);
				$fields = "o_uid as uid,o_nickname as nickname,o_header as header,o_position as position,addtime";
			}
			$this->load->model(array('admin/CardRq_model'=>'do'));
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$items = $this->do->getItems($where,$fields,'addtime desc',$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			if($items){
				foreach ($items as $v){
					$v['addtime'] = time_ago($v['addtime']);
					$news[] = $v;
				}
				AjaxResult_page($news,$pagemenu);
			}else{
				AjaxResult_error('暂无数据~~~~(>_<)~~~~');
			}
		}
	}
	
	function card_top_lists(){//名片广场
		if(is_ajax_request()){
			$data = Posts();	
			$where = '';
			if($data['uid']){
				$where = "uid<>".$data['uid'];
			}
			if($data['phnum']==1){// 1 人气 2点赞 3关注 4附近 
				$order = "rq desc";
			}elseif($data['phnum']==2){
				$order = "zan desc";
			}elseif($data['phnum']==3){
				$order = "follow desc";
			}elseif($data['phnum']==4){
				$scope = calcScope($data['la'], $data['lg'], 500000);////附近50公里范围
				$where .= " and ( la between {$scope['minLat']} and {$scope['maxLat']} ) and ( lg between {$scope['minLng']} and {$scope['maxLng']} ) ";
			}elseif($data['phnum']==5){//默认 最新
				$order = "id desc"; 
			}
			
			if(isset($data['city'])&&$data['city']){
				$where .= " and cid = ".$data['city'];
			}
			
			if(isset($data['industry'])&&$data['industry']){
				$where .= " and iid = ".$data['industry'];
			}
			
			$this->load->model(array('admin/Card_model'=>'do'));
			$page = Posts('page','num');$limit = PAGESIZES;$total = Posts('total','num');
			$items = $this->do->getItems($where,'',$order,$page,$limit,$total,true);
			$pagemenu = $this->do->pagemenu;
			if($items){
				if(isset($data['phnum'])&&$data['phnum']==4){//附近人排序
					foreach ($items as $v){
						$locaArr = calcDistance($data['la'], $data['lg'], $v['la'], $v['lg']);
						$v['sort'] = $locaArr;
						$news_s[] = $v;
						$sort[] = $locaArr;
					}
					array_multisort($sort,SORT_ASC,$news_s);
					$items = $news_s;
				}
				AjaxResult_page(addimg_url($items, 'thumb'),$pagemenu);
			}else{
				AjaxResult_error('暂无数据~~~~(>_<)~~~~');
			}
		}
	}
	
	function card_top_detail(){//名片详情
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Card_model'=>'do'));
			$item = $this->do->getItem(array('id'=>$data['id']),'');
			if($item){
				$this->load->model(array('admin/CardZan_model'=>'zan','admin/CardFollow_model'=>'follow','admin/CardRq_model'=>'rq'));
				$zan = $this->zan->getItem(array('uid'=>$data['uid'],'o_uid'=>$item['uid']));
				if($zan){
					$item['is_zan'] = 1;
				}
				$follow = $this->follow->getItem(array('uid'=>$data['uid'],'o_uid'=>$item['uid']));
				if($follow){
					$item['is_follow'] = 1;
				}
				//执行人气更新
				$rqResult = $this->rq->getItem(array('uid'=>$data['uid'],'o_uid'=>$item['uid']),'id');
				if($rqResult){
					$result = $this->rq->updates(array('addtime'=>time(),'num'=>'+=1'),array('id'=>$rqResult['id']));
				}else{
					$user = $this->do->getItem(array('uid'=>$data['uid']),'uid,username,thumb,position');//获取访问人信息
					$data_add = array(
							'uid'=>$user['uid'],'nickname'=>$user['username'],'header'=>base_url($user['thumb']),'position'=>$user['position'],
							'o_uid'=>$item['uid'],'o_nickname'=>$item['username'],'o_header'=>base_url($item['thumb']),
							'o_position'=>$item['position'],'addtime'=>time(),'num'=>1
					);
					$result = $this->rq->add($data_add);//添加人气列表
				}
								
				//更新人气数量
				if($result){
					$this->do->updates(array('rq'=>"+=1"),array('id'=>$item['id']));
				}
				$item['thumb'] = base_url($item['thumb']);
				AjaxResult(1, '',$item);
			}else{
				AjaxResult_error('名片异常');
			}
		}
	}
	
	function card_top_detail_tie(){//名片详情 中Ta的动态
		if(is_ajax_request()){
			$data = Posts();
			$this->load->model(array('admin/Tie_model'=>'do'));
			$items = $this->do->getItems(array('uid'=>$data['uid']),'id,cname,content,thumb','addtime desc',1,4);
			if($items){
				foreach ($items as $v){
					$v['thumb'] = $v['thumb']?explode(',', $v['thumb']):'';
					$news[] = $v;
				}
				foreach ($news as $v){
					if(is_array($v['thumb'])){
						$thumb = array();
						foreach ($v['thumb'] as $vs){
							$thumb[] = base_url($vs);
						}
					}else{
						$thumb = '';
					}
					$v['thumb'] = $thumb;
					$newss[] = $v;
				}
				AjaxResult(1, '',$newss);
			}else{
				AjaxResult_error('~~~~(>_<)~~~~！暂无数据');
			}
		}
	}
	
	function zan(){
		if(is_ajax_request()){
			$data = Posts();
			$u1 = $data['uid'];$u2 = $data['o_uid'];
			$this->load->model(array('admin/CardZan_model'=>'do','admin/Card_model'=>'card'));
			if($data['query']=="del"){
				$result = $this->do->deletes(array('uid'=>$u1,'o_uid'=>$u2));//被赞删除
				if($result){
					$this->card->updates(array('zan'=>"-=1"),array('uid'=>$u2));
				}
				is_AjaxResult($result);				
			}else{
				$user_result = $this->card->getItems("uid in ($u1,$u2)",'uid,username,thumb,position');//获取被赞人的用户信息
				foreach ($user_result as $v){
					$user[$v['uid']] = $v;
				}
				$data_add = array(
						'uid'=>$u1,'nickname'=>$user[$u1]['username'],'header'=>base_url($user[$u1]['thumb']),'position'=>$user[$u1]['position'],
						'o_uid'=>$u2,'o_nickname'=>$user[$u2]['username'],'o_header'=>base_url($user[$u2]['thumb']),
						'o_position'=>$data['o_position'],'addtime'=>time()
				);
				$result = $this->do->add($data_add);//添加被赞
				//更新名片赞数量
				if($result){
					$this->card->updates(array('zan'=>"+=1"),array('uid'=>$u2));
				}
				is_AjaxResult($result);
			}
			
		}
	}
	
	function follow(){
		if(is_ajax_request()){
			$data = Posts();
			$u1 = $data['uid'];$u2 = $data['o_uid'];
			$this->load->model(array('admin/CardFollow_model'=>'do','admin/Card_model'=>'card'));
			if($data['query']=="del"){
				$result = $this->do->deletes(array('uid'=>$u1,'o_uid'=>$u2));//被赞删除
				if($result){
					$this->card->updates(array('follow'=>"-=1"),array('uid'=>$u2));
				}
				is_AjaxResult($result);
			}else{
				$user_result = $this->card->getItems("uid in ($u1,$u2)",'uid,username,thumb,position');//获取被关注人的用户信息
				foreach ($user_result as $v){
					$user[$v['uid']] = $v;
				}
				$data_add = array(
						'uid'=>$u1,'nickname'=>$user[$u1]['username'],'header'=>base_url($user[$u1]['thumb']),'position'=>$user[$u1]['position'],
						'o_uid'=>$u2,'o_nickname'=>$user[$u2]['username'],'o_header'=>base_url($user[$u2]['thumb']),
						'o_position'=>$data['o_position'],'addtime'=>time()
				);
				$result = $this->do->add($data_add);//添加被赞
				//更新名片关注数量
				if($result){
					$this->card->updates(array('follow'=>"+=1"),array('uid'=>$u2));
				}
				is_AjaxResult($result);
			}
		}
	}
	
	
	

}
