const app = getApp();
var cache = require('../../../utils/cache.js');
Page({

  data: {
    state: 0,
    fy: { page: 1, count: 0, end: 1 }//分页
  },
  onLoad: function (options) {
    var uid = parseInt(wx.getStorageSync('FXID'));
    if (uid) {
      this.setData({ user: wx.getStorageSync('userinfo'), uid: uid, cid: options.cid });
      this.get_lists();
    }

  },
  onReady: function () {

  },
  onShow: function () {
    if (app.reg()) {

    }
  },
  get_lists: function () {
    if (!this.data.fy.end) return false;
    wx.showLoading({ title: '玩命加载中', mask: true });
    var data = {};
    if (this.data.state == 1) {
      data = { state: this.data.state, la: this.data.location.la, cid: this.data.cid, lg: this.data.location.lg, page: this.data.fy.page, total: this.data.fy.count };
    } else {
      data = { state: this.data.state, cid: this.data.cid, page: this.data.fy.page, total: this.data.fy.count };
    }
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_lists',
      method: 'POST',
      data: data,
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ shop: this.data.shop ? this.data.shop.concat(d.data.data) : d.data.data });
        } else {
          this.data.fy.end = 0;
          this.setData({ shop: '', msg: d.data.message });
        } 
        wx.hideLoading();
      }
    });
  },
  onTab: function (e) {
    var index = e.detail.index;
    this.setData({ state: index });
    this.resetFy();
    if (index == 1) {//获取附近的信息/
      var location = cache.get('location');
      if (location) {//本地缓存的地址信息    
        this.setData({ location });
        this.get_lists();
      } else {//获取最新的地址，并执行获取数据
        this.get_location();
      }
    } else {
      this.get_lists();
    }
  },
  get_location() {
    wx.showLoading({
      title: '位置计算中',
    })
    wx.getLocation({
      success: res => {//正常获取    
        var la = res.latitude, lg = res.longitude;
        cache.put('location', { la: la, lg: lg }, 43200);//存入缓存 过期时间1天
        this.setData({ location: { la: la, lg: lg } });
        this.get_lists();
      },
      fail: res => {//用户拒绝，则提示重新获取
        wx.showModal({
          title: '警告',
          content: '此功能无法使用，请允许使用定位',
          success: res1 => {
            if (res1.confirm) {
              wx.openSetting({
                success: res2 => {
                  if (res2.authSetting["scope.userLocation"]) {//如果用户重新同意了授权登录
                    wx.showToast({ title: '授权成功', duration: 2000 });
                    setTimeout(() => {  //要等2秒 才能收到数据
                      this.get_location();
                    }, 2000);
                  }
                }
              })
            } else {//用户再次拒绝，则获取失败
              this.setData({ shop_lists: '', msg: '无法使用此功能!~~~~(>_<)~~~~' });
            }
          }
        })

      },
      complete: function () {
        wx.hideLoading();
      }
    });

  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.data.shop = '';
  },
  navclick: function (e) {
    var cid = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/shop/lists/index?cid=' + cid,
    })
  }
})