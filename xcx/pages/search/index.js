const app = getApp();
Page({
  data: {
    msg:'输入关键字搜索，即可找到您想要的内容'
  },
  onLoad: function (options) {
    this.setData({id:options.id});
  },
  onReady: function () {

  },
  onShow: function () {

  },
  onSearch:function(e){
    var v = e.detail;
    if(v){
      this.setData({ v: v });
      this.get_lists();
    }
    
  },
  get_lists: function () {
    var data = {};
    if (this.data.v) {
      data = { v: this.data.v,search:1 };
    } else {
     return false;
    }
    var url = this.data.id == 1 ? 'wechat/api/tie' : 'wechat/shop_api/shop_lists';
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + url,
      data: data,
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ lists: d.data.data });
        } else {
          this.setData({ lists: '', msg: d.data.message });
        }
        wx.hideLoading();
      }
    })
  }, 
  onMap: function (e) {
    wx.openLocation({
      latitude: parseFloat(e.currentTarget.dataset.la),
      longitude: parseFloat(e.currentTarget.dataset.lg),
      address: e.currentTarget.dataset.name
    })
  },
  onDetail: function (e) {
    var id = e.currentTarget.id
    wx.navigateTo({
      url: '/pages/index/detail/index?id=' + id + "&bottom=" + e.currentTarget.dataset.bottom,
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  }
})