const app = getApp()
Page({
  data: {
    imgLists: [],
    uploadNum: 0,
    lname: '选择您的所在位置',
    pro:0,
    l_info: { "atitude": '', "longitude": '', latitude:'',"name":""},
    mobile:'',
    content: '', lanmu: { cid: 0, cname: '选择发布的栏目'}
  },
  onLoad: function(options) {
    
  },
  onReady: function() {

  },
  onShow: function() {
    if (app.reg()) {
      this.setData({ user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID')) });
      var pages = getCurrentPages();
      var currpage = pages[pages.length - 1];
      if (currpage.data.cat_info) {
        this.setData({ lanmu: currpage.data.cat_info })
      }
    }    
  },
  onSub: function(e) {
    if(this.data.uploadNum){
      this.uploadimg(this.data.imgLists);
    }else{
      this.send('');
    }    
  },
  upload: function() {
    wx.showLoading({ title: '打开相册中', mask: true });
    var that = this;
    wx.chooseImage({
      sizeType: ['compressed'],
      sourceType: ['album'],
      success: function(res) {
        wx.hideLoading();
        var arr = [];
        if (res.tempFilePaths.length == 1) {         
          arr.push(res.tempFilePaths[0]);
        } else if (res.tempFilePaths.length >= 2){
          arr = res.tempFilePaths;
        }
        
        var total = arr.length + that.data.imgLists.length;//判断总共有几张图片 最多9张
        if (total > 10) {
          wx.showToast({
            title: '最多上传9张图片',
            icon: 'none'
          });
        } else {
          arr.map(val => {
            that.data.imgLists.push(val);
          });
          that.setData({
            imgLists: that.data.imgLists,
            uploadNum: that.data.imgLists.length
          });
        }
      },
      complete:function(){
        wx.hideLoading();
      }
    })
  },
  uploadimg: function(data) { 
    
    var that = this, i = data.i ? data.i : 0, success = data.success ? data.success : 0, fail = data.fail ? data.fail : 0, p = 1, thumb = data.thumb?data.thumb:[];
    if (this.data.upimg == 'ok'){
      that.send(thumb);        
      return ;
    }    
    const uploadTask = wx.uploadFile({
      url: app.globalData.host + 'wechat/api/issue_upimg',
      filePath: data[i],
      name: 'file',
      header: {
        "Content-Type": "multipart/form-data", 'X-Requested-With': 'XMLHttpRequest'
      },
      success: (res) => {
        var res_data = JSON.parse(res.data); 
        if(res_data.state==1){
          thumb.push(res_data.data);
        }       
        success++;
      },
      fail: (res) => {
        fail++;
      },
      complete: () => {
        i++;
        if (i == data.length) { //当图片传完时，停止调用
          that.setData({upimg:'ok'});
          that.send(thumb);          
        } else { //若图片还没有传完，则继续调用函数
          data.i = i;
          data.success = success;
          data.fail = fail;
          data.thumb = thumb;
          that.uploadimg(data);
        }
      }
    });
    uploadTask.onProgressUpdate((res) => {
      wx.showLoading({ title: '第 ' + (i+1) + ' 张 ' + res.progress+' %', mask: true });  
    })
  },
  send:function(thumb){
    var that = this;
    if (!that.data.content){
      wx.showToast({
        title: '内容不能为空',
        icon:'none'
      });
      return;
    }
    if (!that.data.lanmu.id) {
      wx.showToast({
        title: '请选择栏目',
        icon: 'none'
      });
      return;
    }
    wx.showLoading({ title: '玩命发布中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/home/issue',
      data: { content: that.data.content, mobile: that.data.mobile,cid:that.data.lanmu.id,cname:that.data.lanmu.cname, thumb: thumb, address_name: that.data.l_info.name, latitude: that.data.l_info.latitude, longitude: that.data.l_info.longitude },
      method: 'POST',
      header: app.header(wx.getStorageSync('PHPSESSID')),
      success: d => {
        app.again(d.data.state);
        if (d.data.state == 1) {
          wx.showModal({
            title: '发布成功',
            showCancel: false,
            content: '分享给朋友或者微信群，让更多的人知道您！',
            success: res => {
              wx.switchTab({
                url: '/pages/index/index',
              })
            }
          })            
        }
        wx.showToast({
          title: d.data.message,icon:'none'
        })
      }
    })
  },
  onClose: function(e) {
    var index = e.currentTarget.id;
    this.data.imgLists.splice(index, 1);
    this.setData({
      imgLists: this.data.imgLists,
      uploadNum: this.data.imgLists.length
    });
  },
  onContent: function (e){
    this.setData({content:e.detail});
  },
  onMobile: function (e) {    
    this.setData({ mobile: e.detail });
  },
  onMap: function() {
    var that = this;
    wx.chooseLocation({
      success: function(res) {
        if (res.errMsg == 'chooseLocation:ok' && res.address) {
          that.setData({
            l_info: res
          });
        }
      },
    })
  }
})